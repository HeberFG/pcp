package utez.edu.mx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import utez.edu.mx.entity.Color;
import utez.edu.mx.service.ColorService;

@RestController
@RequestMapping("/pcp")
@CrossOrigin(origins = "http://localhost:8080")
public class ColorController {
	
	@Autowired
	private ColorService colorService;
	
	@GetMapping("/colores")
	public List<Color> getAll(){
		return colorService.getAll();
	}
}
