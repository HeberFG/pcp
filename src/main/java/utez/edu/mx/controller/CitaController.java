package utez.edu.mx.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import utez.edu.mx.entity.Cita;
import utez.edu.mx.service.CitaService;

@RestController
@RequestMapping("/pcp")
@CrossOrigin(origins = "http://localhost:8080")
public class CitaController {
	
	@Autowired
	private CitaService citaService;
	
	@GetMapping("/citas")
	public List<Cita> getAll() {
		return citaService.getAll();
	}
	
	@GetMapping("/citas/confirm")
	public List<Cita> getConfirm() {
		return citaService.getConfirm();
	}
	
	@GetMapping("/citas/unconfirm")
	public List<Cita> getUnconfirm() {
		return citaService.getUnconfirm();
	}
	
	@GetMapping("/citas/{id}")
	public Cita getDate(@PathVariable("id") Long id) {
		return citaService.getCita(id);
	}
	
	@PostMapping("/citas/vestido")
	public Cita clientVestido (@RequestParam("nombre") String nombre, @RequestParam("telefono") String telefono, @RequestParam("vestido") Long vestido) throws IOException{
		MultipartFile foto = null;
		return citaService.createPetition(nombre, telefono, vestido, foto);
	}
	
	@PostMapping("/citas/foto")
	public Cita clientFoto (@RequestParam("nombre") String nombre, @RequestParam("telefono") String telefono, @RequestParam("foto") MultipartFile foto) throws IOException{
		Long vestido = null;
		return citaService.createPetition(nombre, telefono, vestido, foto);
	}
	
	@PutMapping("/citas")
	public Cita createAdmin (@RequestParam("id") Long id, @RequestParam("fechaCita") Date fechaCita, @RequestParam("fechaEntrega") Date fechaEntrega, @RequestParam("talla") Long talla) throws IOException{
		return citaService.createAdmin(id, fechaCita, fechaEntrega, talla);
	}
}
