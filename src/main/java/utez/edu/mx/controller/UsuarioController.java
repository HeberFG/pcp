package utez.edu.mx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import utez.edu.mx.entity.Usuario;
import utez.edu.mx.service.UsuarioService;

@RestController
@RequestMapping("/pcp")
@CrossOrigin(origins = "http://localhost:8080")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/usuarios")
	public List<String> getUsers(){
		return usuarioService.getAll();
	}
	
	@PostMapping("/usuario")
	public Boolean create(@RequestBody Usuario usuario) {
		return usuarioService.create(usuario);
	}
	
	@PutMapping("/usuario")
	public Boolean cambiarContra(@RequestParam("id") Long id, @RequestParam("contra") String contra, @RequestParam("newContra") String newContra) {
		return usuarioService.cambioContra(id, contra, newContra);
	}
}
