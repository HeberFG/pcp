package utez.edu.mx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import utez.edu.mx.entity.ColorDisponible;
import utez.edu.mx.service.ColorDisponibleService;

@RestController
@RequestMapping("/pcp")
@CrossOrigin(origins = "http://localhost:8080")
public class ColorDisponibleController {
	
	@Autowired
	private ColorDisponibleService colorDisponibleService;
	
	@GetMapping("/coloresdis")
	public List<ColorDisponible> getAll(){
		return colorDisponibleService.getAll();
	}
	
	@GetMapping("/coloresdis/{id}")
	public List<ColorDisponible> getForDress(@PathVariable("id") Long id){
		return colorDisponibleService.getAllForDress(id);
	}
}
