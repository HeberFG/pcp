package utez.edu.mx.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import utez.edu.mx.entity.Producto;
import utez.edu.mx.service.ProductoService;

@RestController
@RequestMapping("/pcp")
@CrossOrigin(origins = "http://localhost:8080")
public class ProductoController {

	@Autowired
	private ProductoService productoService;
	
	@GetMapping("/productos")
	public List<Producto> getProductos(){
		return productoService.getAll();
	}
	
	@GetMapping("/productos/{id}")
	public Producto getProduct (@PathVariable("id") Long id){
		return productoService.getProducto(id);
	}
	
	@PostMapping("/productos")
	public Producto save (@RequestParam("nombre") String nombre , @RequestParam("marca") String marca,@RequestParam("foto") MultipartFile foto, @RequestParam("precio") Double precio) throws IOException{
		return productoService.create(nombre, marca , foto, precio);
	}
	
	@PutMapping("/productos")
	public Producto update (@RequestParam("id") Long id, @RequestParam("nombre") String nombre , @RequestParam("marca") String marca,@RequestParam("foto") MultipartFile foto, @RequestParam("precio") Double precio) throws IOException{
		return productoService.saveOrUpdate(id, nombre, marca, foto, precio);
	}
	
	@PutMapping("/productos/disable/{id}")
	public Boolean disable(@PathVariable("id") Long id) {
		return productoService.disable(id);
	}
}
