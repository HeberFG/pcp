package utez.edu.mx.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import utez.edu.mx.entity.Bata;
import utez.edu.mx.entity.Pedido;
import utez.edu.mx.service.PedidoService;

@RestController
@RequestMapping("/pcp")
@CrossOrigin(origins = "http://localhost:8080")
public class PedidoController {

	@Autowired
	private PedidoService pedidoSerivice;
	
	@GetMapping("/pedidos")
	public List<Pedido> getAll() {
		return pedidoSerivice.getAll();
	}
	
	@GetMapping("/pedidos/confirm")
	public List<Pedido> getConfirm() {
		return pedidoSerivice.getConfirm();
	}
	
	@GetMapping("/pedidos/unconfirm")
	public List<Pedido> getUnconfirm() {
		return pedidoSerivice.getUnconfirm();
	}
	
	@GetMapping("/pedidos/{id}")
	public Pedido pedidoById(@PathVariable("id") Long id){
		return pedidoSerivice.getById(id);
	}
	
	@PostMapping("/pedidos/bata")
	public Pedido create(@RequestParam("nombre") String nombre, @RequestParam("telefono") String telefono, @RequestParam("numeroPiezas") Integer numeroPiezas, @RequestParam("bata") Bata bata) {
		Long idProducto = null;
		return pedidoSerivice.create(nombre, telefono, numeroPiezas, idProducto, bata);
	}
	
	@PostMapping("/pedidos/producto")
	public Pedido create(@RequestParam("nombre") String nombre, @RequestParam("telefono") String telefono, @RequestParam("numeroPiezas") Integer numeroPiezas, @RequestParam("idProducto") Long idProducto) {
		Bata bata = null;
		return pedidoSerivice.create(nombre, telefono, numeroPiezas, idProducto, bata);
	}
	
	@PutMapping("/pedidos/confirm/{id}")
	public Boolean confirm(@PathVariable("id") Long id) {
		return pedidoSerivice.confirm(id);
	}
	
	@DeleteMapping("/pedidos/{id}")
	public void delete(@PathVariable("id") Long id) {
		pedidoSerivice.remove(id);
	}
}
