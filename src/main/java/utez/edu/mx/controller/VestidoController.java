package utez.edu.mx.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import utez.edu.mx.entity.Vestido;
import utez.edu.mx.service.VestidoService;

@RestController
@RequestMapping("/pcp")
@CrossOrigin(origins = "http://localhost:8080")
public class VestidoController {

	@Autowired
	private VestidoService vestidoService;
	
	@GetMapping("/vestidos")
	public List<Vestido> getVestidos(){
		return vestidoService.getAll();
	}
	
	@GetMapping("/vestidos/{id}")
	public Vestido getVestido(@PathVariable("id") Long id) {
		return vestidoService.getVestido(id);
	}
	
	@PostMapping("/vestidos")
	public Vestido save(@RequestParam("nombre") String nombre, @RequestParam("foto") MultipartFile foto, @RequestParam("costo") double costo, @RequestParam("tipo") String tipo, @RequestParam("colores") String[] colores) throws IOException {
		return vestidoService.create(nombre, foto, costo, tipo, colores);
	}
	
	@PutMapping("/vestidos")
	public Vestido update(@RequestParam("id") Long id ,@RequestParam("nombre") String nombre, @RequestParam("foto") MultipartFile foto, @RequestParam("costo") double costo, @RequestParam("estado") Boolean estado, @RequestParam("colores") String[] colores) throws IOException {
		return vestidoService.saveOrUpdate(id ,nombre, foto, costo, estado, colores);
	}
	
	@PutMapping("vestidos/disable/{id}")
	public Boolean disable(@PathVariable("id") Long id) {
		return vestidoService.disable(id);
	}
}
