package utez.edu.mx.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import utez.edu.mx.entity.Producto;
import utez.edu.mx.repository.ProductoRepository;

@Service
public class ProductoService {
	@Autowired
	private ProductoRepository productoRepository;
	
	public List<Producto> getAll(){
		return productoRepository.findAll();
	}
	
	public Producto getProducto(long id) {
		return productoRepository.findById(id).get();
	}
	
	public Producto create(String nombre, String marca, MultipartFile foto, Double precio) throws IOException{
		
		//Se construye un Objecto de tipo Producto esperando los valores correspondientes en un formdata
		Producto productoForInsert = Producto.builder()
				.nombre(nombre)
				.marca(marca)
				.foto(foto.getBytes())
				.precio(precio)
				.estado(true)
				.build();
		
		Producto producto = null;
		
		producto = productoRepository.save(productoForInsert);
		
		if(producto != null) {
			return producto;
		}
		
		return null;
	}
	
	public Producto saveOrUpdate(Long id, String nombre, String marca, MultipartFile foto, Double precio) throws IOException{
		
		//Se valida si exitse el producto
		Producto validateProductExist= getProducto(id);
		
		Producto productoForInsert = null;
	
		Producto producto = null;
		
		if (validateProductExist != null) {
			
			//Si se actializa la foto
			if (foto != null) {
				productoForInsert = Producto.builder()
					.nombre(nombre)
					.marca(marca)
					.foto(foto.getBytes())
					.precio(precio)
					.build();
			}else {
				
				productoForInsert = Producto.builder()
						.nombre(nombre)
						.marca(marca)
						.precio(precio)
						.build();
				productoForInsert.setFoto(validateProductExist.getFoto());
			}
			
			productoForInsert.setIdProduct(id);
			
			producto = productoRepository.save(productoForInsert);
		
			if(producto != null) {
				return producto;
			}
		}
		return null;
	}
	
	public Boolean disable(Long id) {
		
		Producto producto = getProducto(id);
		
		if (producto != null) {
			producto.setEstado(!producto.getEstado());
			productoRepository.save(producto);
			return true;
		}
		return false;
	} 
}