package utez.edu.mx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Largo;
import utez.edu.mx.repository.LargoRepository;

@Service
public class LargoService {
	@Autowired
	private LargoRepository largoRepository;
	
	public List<Largo> getAll(){
		return largoRepository.findAll();
	}
	
	public Largo getLargo(Long id) {
		return largoRepository.findById(id).get();
	}
}
