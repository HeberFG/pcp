package utez.edu.mx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Bata;
import utez.edu.mx.entity.Pedido;
import utez.edu.mx.entity.Producto;
import utez.edu.mx.repository.PedidoRepositry;

@Service
public class PedidoService {
	
	@Autowired
	private PedidoRepositry pedidoRepository;
	
	@Autowired
	private BataService bataService;
	
	@Autowired
	private ProductoService productoService;
	
	public List<Pedido> getAll(){
		return pedidoRepository.findAll();
	}
	
	public List<Pedido> getConfirm(){
		
		List<Pedido> allPedidos = getAll();
		
		List<Pedido> pedidos = new ArrayList<Pedido>();
		
		for (Pedido pedido : allPedidos) {
			if(pedido.getEstado()) {
				pedidos.add(pedido);
			}
		}
		return pedidos;
	}
	
	public List<Pedido> getUnconfirm(){
		List<Pedido> allPedidos = getAll();
		
		List<Pedido> pedidos = new ArrayList<Pedido>();
		
		for (Pedido pedido : allPedidos) {
			if(!pedido.getEstado()) {
				pedidos.add(pedido);
			}
		}
		
		return pedidos;
	}
	
	public Pedido getById(Long id){
		return pedidoRepository.findById(id).get();
	}
	
	public Pedido create(String nombre, String telefono, Integer numeroPiezas, Long idProducto, Bata idBata) {
		
		Pedido pedidoForInsert = null, pedido = null;
		Bata bata;
		if(idBata != null) {
			Double costo = 300.0 * numeroPiezas;
			pedidoForInsert = Pedido.builder().nombre(nombre).telefono(telefono).costo(costo).numeroPiezas(numeroPiezas).build();
			
			bata =  bataService.seveOrUpdate(idBata);
			
			pedidoForInsert.setBata(bata);
			
		}else {
			Producto producto = productoService.getProducto(idProducto);
			Double costo = producto.getPrecio() * numeroPiezas;
			pedidoForInsert = Pedido.builder().nombre(nombre).telefono(telefono)
					.costo(costo).numeroPiezas(numeroPiezas).producto(producto).build();
		
		}
		pedidoForInsert.setEstado(false);
		
		pedido = pedidoRepository.save(pedidoForInsert);
		
		if (pedido != null) {
			return pedido;
		}
		
		return null;
	}
	
	public Boolean confirm(Long id) {
		Pedido pedido = getById(id);
		if (pedido != null) {
			pedido.setEstado(true);
			return true;
		}
		return false;
	}
	
	public void remove(Long id) {
		pedidoRepository.deleteById(id);
	}
}