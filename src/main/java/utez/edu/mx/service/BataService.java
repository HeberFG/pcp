package utez.edu.mx.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Bata;
import utez.edu.mx.repository.BataRepository;

@Service
public class BataService {
	
	@Autowired
	private BataRepository bataRepository;
	
	public Bata getBata(Long id) {
		return bataRepository.findById(id).get();
	}
	
	public Bata seveOrUpdate(Bata bata) {
		return bataRepository.save(bata);
	}
	
	public void remove(Long id) {
		bataRepository.deleteById(id);
	}
}