package utez.edu.mx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Usuario;
import utez.edu.mx.repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public List<String> getAll(){
		List<String> usersNames = new ArrayList<String>();
		List<Usuario> users = usuarioRepository.findAll();
		for (Usuario user : users) {
			usersNames.add(user.getNombreUsuario());
		}
		return usersNames;
	}
	
	public Boolean create(Usuario usuario) {
		usuarioRepository.save(usuario);
		return true;
	}
	
	public Boolean cambioContra(Long id, String contra, String newContra) {
		Usuario usuario = usuarioRepository.findById(id).get();
		if(usuario.getContrasenia().equals(contra)) {
			usuario.setContrasenia(newContra);
			usuarioRepository.save(usuario);
			return true;
		}
		return false;
	}
}
