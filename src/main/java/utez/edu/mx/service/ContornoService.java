package utez.edu.mx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Contorno;
import utez.edu.mx.repository.ContornoRepository;

@Service
public class ContornoService {
	
	@Autowired
	private ContornoRepository contornoRepository;
	
	public List<Contorno> getAll(){
		return contornoRepository.findAll();
	}
	
	public Contorno getContorno(Long id) {
		return contornoRepository.findById(id).get();
	}
}
