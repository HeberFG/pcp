package utez.edu.mx.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.ColorDisponible;
import utez.edu.mx.repository.ColorDisponibleRepository;

@Service
public class ColorDisponibleService {
	
	@Autowired
	private ColorDisponibleRepository colorDisponRepository;
	
	public List<ColorDisponible> getAll() {
		return colorDisponRepository.findAll();
	}
	
	public List<ColorDisponible> getAllForDress(Long idVestido){
		List<ColorDisponible> allColors = colorDisponRepository.findAll();
		
		List<ColorDisponible> colors = new ArrayList<ColorDisponible>();
		
		for (ColorDisponible colorDisponible : allColors) {
			if (colorDisponible.getIdVestido().getId() == idVestido) {
				colors.add(colorDisponible);
			}
		}
		
		return colors;
	}
	
	public ColorDisponible create(ColorDisponible colorDisponible) {
		
		List<ColorDisponible> colors = getAll();
		
		Boolean create = true;
		
		ColorDisponible response = null;
		
		for (ColorDisponible colorDisponible2 : colors) {
			if (colorDisponible.getIdVestido().equals(colorDisponible2.getIdVestido()) && colorDisponible.getIdColor().equals(colorDisponible2.getIdColor())) {
				create = false;
				response = colorDisponible2;
			}
		}
		
		if (create) {
			response = colorDisponRepository.save(colorDisponible);
		}
		
		return response;
	}
	
	public void delete(Long id) {
		colorDisponRepository.deleteById(id);
	}
}
