package utez.edu.mx.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import utez.edu.mx.entity.Cita;

import utez.edu.mx.repository.CitaRepository;

@Service
public class CitaService {

	@Autowired
	private CitaRepository citaRepository;
	
	@Autowired
	private VestidoService vestidoService;
	
	@Autowired TallaService tallaService;	
	
	public List<Cita> getAll(){
		return citaRepository.findAll();
	}
	
	public Cita getCita(Long id) {
		return citaRepository.findById(id).get();
	}
	
	public Cita createPetition(String nombre, String telefono, Long vestido, MultipartFile foto) throws IOException {
		
		Cita citaForInsert = null, cita = null;
		
		if (foto != null) {
			citaForInsert = Cita.builder().nombre(nombre).telefono(telefono).vestido(null).foto(foto.getBytes()).build();
			citaForInsert.setEstado(false);
			cita = citaRepository.save(citaForInsert);
		}else {
			citaForInsert = Cita.builder().nombre(nombre).telefono(telefono).vestido(vestidoService.getVestido(vestido)).build();
			citaForInsert.setEstado(false);
			cita = citaRepository.save(citaForInsert);
		}
		
		if (cita != null) {			
			return cita;
		}
		
		return null;
	}
	
	public List<Cita> getConfirm(){
		
		List<Cita> allCitas = getAll();
		
		List<Cita> citas = new ArrayList<Cita>();
		
		for (Cita cita : allCitas) {
			if (cita.getEstado()) {
				citas.add(cita);
			}
		}
		
		return citas;
	}
	
public List<Cita> getUnconfirm(){
		
		List<Cita> allCitas = getAll();
		
		List<Cita> citas = new ArrayList<Cita>();
		
		for (Cita cita : allCitas) {
			if (!cita.getEstado()) {
				citas.add(cita);
			}
		}
		
		return citas;
	}
	
	public Cita createAdmin(Long id, Date fechaCita, Date fechaEntrega, Long talla) throws IOException {
		
		Cita citaForInsert = null, cita = null, citaExist = null;
		
		citaExist = getCita(id);
		
		if(citaExist != null){
			
			citaForInsert = citaExist;
			
			citaForInsert.setFechaCita(fechaCita);
			
			citaForInsert.setFechaEntrega(fechaEntrega);
			
			citaForInsert.setTalla(tallaService.getTalla(talla));
			
			citaForInsert.setEstado(true);
			
			cita = citaRepository.save(citaForInsert);
		}
		
		if (cita != null) {
			return cita;
		}
		
		return null;
	}
	
	public void delete(Long id) {
		citaRepository.deleteById(id);
	}
}
