package utez.edu.mx.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import utez.edu.mx.entity.Color;
import utez.edu.mx.entity.ColorDisponible;
import utez.edu.mx.entity.Vestido;
import utez.edu.mx.repository.VestidoRepository;

@Service
public class VestidoService {

	@Autowired
	private VestidoRepository vestidoRepository;
	
	@Autowired
	private ColorService colorService;
	
	@Autowired
	private ColorDisponibleService colorDisponibleService;
	
	public List<Vestido> getAll(){
		return vestidoRepository.findAll();
	}
	
	public Vestido getVestido(Long id) {
		return vestidoRepository.findById(id).get();
	}
	
	public Vestido create(String nombre, MultipartFile foto, double costo,String tipo, String[] colores) throws IOException {
		
		Vestido vestidoForInsert = Vestido.builder()
				.nombre(nombre)
				.foto(foto.getBytes())
				.costo(costo)
				.tipo(tipo).build();
		vestidoForInsert.setEstado(true);
		Vestido vestido = null;
		
		vestido = vestidoRepository.save(vestidoForInsert);
		
		for (String color : colores) {
			Color colorInsert = new Color();
			ColorDisponible colorDis = new ColorDisponible();
			
			if (colorService.colorExist(color) == null) {
				
				colorInsert.setCode(color);
				
				colorDis.setIdColor(colorService.create(colorInsert));
				
				colorDis.setIdVestido(vestido);
				
				colorDisponibleService.create(colorDis);
			}else {
				colorDis.setIdColor(colorService.colorExist(color));
				colorDis.setIdVestido(vestido);
				colorDisponibleService.create(colorDis);
			}
		}
		
		if (vestido != null) {
			return vestido;
		}
		
		return null;
	}
	
public Vestido saveOrUpdate(Long id, String nombre, MultipartFile foto, double costo, Boolean estado, String[] colores) throws IOException {
		
		Vestido vestidoForInsert = null, vestido = null, validateExist = getVestido(id);
		
		if (validateExist != null) {
			
			if (foto != null) {
				
				vestidoForInsert = Vestido.builder()
						.nombre(nombre)
						.foto(foto.getBytes())
						.costo(costo)
						.estado(estado).build();
				
			}else {
				vestidoForInsert = Vestido.builder()
						.nombre(nombre)
						.costo(costo)
						.estado(estado).build();
				vestidoForInsert.setFoto(validateExist.getFoto());
			}
			
			vestidoForInsert.setId(id);
			
			vestido = vestidoRepository.save(vestidoForInsert);
			
			for (String color : colores) {
				Color colorInsert = new Color();
				ColorDisponible colorDis = new ColorDisponible();
				if (colorService.colorExist(color) == null) {
					
					colorInsert.setCode(color);
					colorDis.setIdColor(colorService.create(colorInsert));
					
					colorDis.setIdVestido(vestido);
					
					colorDisponibleService.create(colorDis);
				}else {
					colorDis.setIdColor(colorService.colorExist(color));
					colorDis.setIdVestido(vestido);
					colorDisponibleService.create(colorDis);
				}
			}
			
			if (vestido != null) {
				return vestido;
			}
		}
		return null;
	}

public Boolean disable(Long id) {
	Vestido vestido = getVestido(id);
	vestido.setEstado(!vestido.getEstado());
	Vestido vestidoResponse = vestidoRepository.save(vestido);
	if (vestidoResponse != null) {
		return true;
	}
	return false;
}
}
