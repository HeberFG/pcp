package utez.edu.mx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Color;
import utez.edu.mx.repository.ColorRepository;

@Service
public class ColorService {
	
	@Autowired
	private ColorRepository colorRepository;
	
	public List<Color> getAll(){
		return colorRepository.findAll();
	}
	
	public Color getColor(Long id) {
		return colorRepository.findById(id).get();
	}
	
	public Color colorExist(String code) {
		List<Color> colors = getAll();
		Color exist = null;
		for (Color color : colors) {
			if(color.getCode().equals(code)) {
				exist = color;
			}
		}
		return exist;
	}
	
	public Color create(Color color) {
		return colorRepository.save(color);
	}
	
	public Color update(Color color) {
		return colorRepository.save(color);
	}
	
	public void delete(Long id) {
		colorRepository.deleteById(id);
	}
}
