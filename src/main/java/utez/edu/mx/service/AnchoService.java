package utez.edu.mx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Ancho;
import utez.edu.mx.repository.AnchoRepository;

@Service
public class AnchoService {
	@Autowired
	private AnchoRepository anchoRepository;
	
	public List<Ancho> getAll(){
		return anchoRepository.findAll();
	}
	
	public Ancho getAncho(Long id) {
		return anchoRepository.findById(id).get();
	}
}
