package utez.edu.mx.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utez.edu.mx.entity.Talla;
import utez.edu.mx.repository.TallaRepository;

@Service
public class TallaService {
	
	@Autowired
	private TallaRepository tallaRepository;
	
	public List<Talla> getAll(){
		return tallaRepository.findAll();
	}
	
	public Talla getTalla(Long id) {
		return tallaRepository.findById(id).get();
	}
}
