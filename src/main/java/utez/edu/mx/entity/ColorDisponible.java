package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name= "color_disponible")
@Data
public class ColorDisponible implements Serializable{

	private static final long serialVersionUID = 1493510518905065895L;
 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "id_vestido")
	private Vestido idVestido;
	
	@ManyToOne
	@JoinColumn(name = "id_color")
	private Color idColor;
}