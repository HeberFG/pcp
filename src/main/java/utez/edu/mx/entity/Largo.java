package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "largo")
@Data
public class Largo implements Serializable{
	
	private static final long serialVersionUID = -2563959913369028237L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "largo_talle_delantero")
	private Float largoTalleDelantero;
	
	@Column(name = "altura_pecho")
	private Float alturaPecho;
	
	@Column(name = "largo_espalda")
	private Float largoEspalda;
}
