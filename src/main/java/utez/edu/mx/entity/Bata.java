package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity
@Table(name = "bata")
@Data
public class Bata implements Serializable{

	private static final long serialVersionUID = 5011409666957634654L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "color")
	@NotNull(message = "Debe seleccionar un color")
	private String color;
	
	@Column(name = "color_bordado")
	@NotNull(message = "Debe seleccionar un color de bordado")
	private String colorBordado;
	
	@Column(name = "leyenda")
	@NotNull(message = "Debe ingresar una leyenda")
	private String leyenda;
	
	@Column(name = "talla")
	private String talla;
	
	@Column(name = "costo")
	private double costo;
}
