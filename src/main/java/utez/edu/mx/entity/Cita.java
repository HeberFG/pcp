package utez.edu.mx.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "cita")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Cita implements Serializable{
	
	private static final long serialVersionUID = 4711487868317247730L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nombre_cliente")
	private String nombre;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "fecha_cita")
	private Date fechaCita;
	
	@Column(name = "fecha_entrega")
	private Date fechaEntrega;
	
	@Column(name = "foto")
	@Lob
	private byte[] foto;
	
	@Column(name="estado")
	private Boolean estado;
	
	@ManyToOne
	@JoinColumn(name = "id_vestido")
	private Vestido vestido;
	
	@ManyToOne
	@JoinColumn(name = "id_talla")
	private Talla talla;
}
