package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "vestido")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Vestido implements Serializable{

	private static final long serialVersionUID = 7173011358047102123L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nombre")
	@NotNull(message = "Debe asignar un nombre")
	private String nombre;
	
	@Column(name = "foto")
	@Lob
	private byte [] foto;
	
	@Column(name = "costo")
	@NotNull(message = "Debe asignar un precio")
	private double costo;
	
	@Column(name = "estado")
	private Boolean estado;
	
	@Column(name = "tipo")
	private String tipo;
}
