package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "contorno")
@Data
public class Contorno implements Serializable{
	
	private static final long serialVersionUID = -9146332747975760390L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "contorno_brazo")
	private Float contornoBrazo;
	
	@Column(name = "contorno_cuello")
	private Float contornoCuello;
	
	@Column(name = "contorno_pecho")
	private Float contornoPecho;
	
	@Column(name = "contorno_cintura")
	private Float contornoCintura;
	
	@Column(name = "contorno_cadera")
	private Float contornoCadera;
}
