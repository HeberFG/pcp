package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="pedido")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Pedido implements Serializable {

	private static final long serialVersionUID = 7234760601384682750L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "telefono")
	private String telefono;

	@Column(name = "costo")
	private Double costo ;
	
	@Column(name = "numero_piezas")
	private Integer numeroPiezas;
	
	@Column(name = "estado")
	private Boolean estado;
	
	@ManyToOne
	@JoinColumn(name="id_producto")
	private Producto producto;
	
	@ManyToOne
	@JoinColumn(name="id_bata")
	private Bata bata;
}
