package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "talla")
@Data
public class Talla implements Serializable{
	
	private static final long serialVersionUID = 8282412244907778078L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nombre")
	private String nombre;
	
	@OneToOne
	@JoinColumn(name = "id_contorno")
	private Contorno contorno;
	
	@OneToOne
	@JoinColumn(name = "id_largo")
	private Largo largo;
	
	@OneToOne
	@JoinColumn(name = "id_ancho")
	private Ancho ancho;
	
}
