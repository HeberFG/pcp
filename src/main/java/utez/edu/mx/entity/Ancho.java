package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "ancho")
@Data
public class Ancho implements Serializable{
	
	private static final long serialVersionUID = -3172355087357370816L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "ancho_espalda")
	private Float anchoEspalda;
	
	@Column(name = "ancho_hombros")
	private Float anchoHombros;
}
