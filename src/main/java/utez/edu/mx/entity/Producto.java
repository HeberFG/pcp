package utez.edu.mx.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="producto")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Producto implements Serializable {
	
	private static final long serialVersionUID = 5222002676282556230L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long idProduct;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="marca")
	private String marca;
	
	@Column(name="foto")
	@Lob
	private byte [] foto;
	
	@Column(name = "estado")
	private Boolean estado;
	
	@Column(name="precio")
	private double precio;
}
