package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Cita;

@Repository
public interface CitaRepository extends JpaRepository<Cita, Long>{

}
