package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Talla;

@Repository
public interface TallaRepository extends JpaRepository<Talla, Long>{

}
