package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Color;

@Repository
public interface ColorRepository extends JpaRepository<Color, Long>{

}
