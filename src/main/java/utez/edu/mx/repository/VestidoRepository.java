package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Vestido;

@Repository
public interface VestidoRepository extends JpaRepository<Vestido, Long>{

}
