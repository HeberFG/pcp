package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Bata;

@Repository
public interface BataRepository extends JpaRepository<Bata, Long>{

}
