package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Pedido;

@Repository
public interface PedidoRepositry extends JpaRepository<Pedido, Long> {

}
