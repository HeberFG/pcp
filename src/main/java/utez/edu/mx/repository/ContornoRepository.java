package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Contorno;

@Repository
public interface ContornoRepository extends JpaRepository<Contorno, Long>{

}
