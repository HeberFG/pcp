package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Ancho;

@Repository
public interface AnchoRepository extends JpaRepository<Ancho, Long> {
	
}
