package utez.edu.mx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import utez.edu.mx.entity.Largo;

@Repository
public interface LargoRepository extends JpaRepository<Largo, Long> {

}
